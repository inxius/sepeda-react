import { images } from "./assets"
import {Navbar, DCarousel, DSepeda, DModal, DFooter, DNavigationBtn} from "./components/desktop.components"
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';

function DesktopView() {
    return (
        <>
        <Navbar />

        <DCarousel 
            img1={images.DBanner1}
            img2={images.DBanner2}
            img3={images.DBanner3}
        />

        <DSepeda id="road" bike="road" bg="bg-sky-100" mlink1="road-1" mlink2="road-2" mlink3="road-3" />
        <DSepeda id="mountain" bike="mountain" mlink1="mountain-1" mlink2="mountain-2" mlink3="mountain-3" />
        <DSepeda id="folding" bike="folding" bg="bg-sky-100" mlink1="folding-1" mlink2="folding-2" mlink3="folding-3" />

        <DModal id="road-1" back="#road" bike="road" urut="s1" />
        <DModal id="road-2" back="#road" bike="road" urut="s2" />
        <DModal id="road-3" back="#road" bike="road" urut="s3" />
        <DModal id="mountain-1" back="#mountain" bike="mountain" urut="s1" />
        <DModal id="mountain-2" back="#mountain" bike="mountain" urut="s2" />
        <DModal id="mountain-3" back="#mountain" bike="mountain" urut="s3" />
        <DModal id="folding-1" back="#folding" bike="folding" urut="s1" />
        <DModal id="folding-2" back="#folding" bike="folding" urut="s2" />
        <DModal id="folding-3" back="#folding" bike="folding" urut="s3" />

        <DNavigationBtn link="#nav" icon={faArrowUp} />
        <DFooter />
        </>
    )
}

export default DesktopView