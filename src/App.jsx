import './App.css'
import { BreakpointProvider, Breakpoint } from 'react-socks'
import {MobileMainView, MBikeView, MBikeDetailView} from './Mobile'
import DesktopView from './Desktop'
import ErrorPage from './error-page'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

function App() {
	const MRouter = createBrowserRouter([
		{
			path: "/",
			element: <MobileMainView />,
			errorElement: <ErrorPage />,
		},
		{
			path: "/:bike",
			element: <MBikeView />,
			errorElement: <ErrorPage />,
		},
		{
			path: "/:bike/:id",
			element: <MBikeDetailView />,
			errorElement: <ErrorPage />,
		},
	])

	const DRouter = createBrowserRouter([
		{
			path: "/",
			element: <DesktopView />,
			errorElement: <ErrorPage />,
		},
	])

	return (
		<BreakpointProvider>
			<Breakpoint small down>
				<RouterProvider router={MRouter} />
			</Breakpoint>

			<Breakpoint medium up>
				<RouterProvider router={DRouter} />
			</Breakpoint>
		</BreakpointProvider>
	)
}

export default App
