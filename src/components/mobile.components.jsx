import {images} from '../assets/index';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Navbar = () => {
    return (
        <>
        <div className='w-full flex justify-center p-2 bg-sky-100' id='nav'>
            <Link to='/'>
                <img className='w-[50px]' src={images.ImgNavbar} alt="Logo Sepeda" />
            </Link>
        </div>
        </>
    )
}

const Footer = () => {
    return (
        <>
        <footer className="footer footer-center p-10 bg-primary text-primary-content mt-5">
            <div>
                <img className='w-[50px]' src={images.ImgNavbar} alt="" />
                <p className="font-bold">
                Website ini ditujukan hanya untuk portopolio. <br/>Logo, Gambar dan Deskripsi diambil dari <a href="https://elementbike.id" target="_blank">Element Bike</a>
                </p> 
                {/* <p>Copyright © 2023 - All right reserved</p> */}
            </div> 
            <div>
                <div className="grid grid-flow-col gap-4">
                <a href='https://twitter.com/_jafarsodiq'><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-current"><path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"></path></svg></a> 
                <a><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-current"><path d="M22.23 0H1.77C.8 0 0 .77 0 1.72v20.56C0 23.23.8 24 1.77 24h20.46c.98 0 1.77-.77 1.77-1.72V1.72C24 .77 23.2 0 22.23 0zM7.27 20.1H3.65V9.24h3.62V20.1zM5.47 7.76h-.03c-1.22 0-2-.83-2-1.87 0-1.06.8-1.87 2.05-1.87 1.24 0 2 .8 2.02 1.87 0 1.04-.78 1.87-2.05 1.87zM20.34 20.1h-3.63v-5.8c0-1.45-.52-2.45-1.83-2.45-1 0-1.6.67-1.87 1.32-.1.23-.11.55-.11.88v6.05H9.28s.05-9.82 0-10.84h3.63v1.54a3.6 3.6 0 0 1 3.26-1.8c2.39 0 4.18 1.56 4.18 4.89v6.21z"/></svg></a>
                </div>
            </div>
        </footer>
        </>
    )
}

const Box = (props) => {
    return (
        <>
        <Link to={props.link}>
            <div className='w-[150px] h-[150px] border-2 border-solid border-primary rounded-lg'>
                <img src={props.img} alt={props.alt} />
            </div>
        </Link>
        </>
    )
}

const Box2 = (props) => {
    return (
        <>
        <Link to={props.link}>
            <div className='flex flex-col items-center'>
                <div className='w-[200px] h-[200px] border-2 border-solid border-primary-focus rounded-lg'>
                    <img src={props.img} alt={props.alt} />
                </div>
                <p className='font-semibold text-base'>{props.title}</p>
            </div>
        </Link>
        </>
    )
}

const MCarousel = (props) => {
    return (
        <>
        <div className="carousel w-[300px]">
            <div id="item1" className="carousel-item w-full">
                <img src={props.img1} className="w-full" />
            </div> 
            <div id="item2" className="carousel-item w-full">
                <img src={props.img2} className="w-full" />
            </div> 
            <div id="item3" className="carousel-item w-full">
                <img src={props.img3} className="w-full" />
            </div>
        </div> 
        <div className="flex justify-center w-full py-2 gap-2">
            <a href="#item1" className="btn btn-xs">1</a> 
            <a href="#item2" className="btn btn-xs">2</a> 
            <a href="#item3" className="btn btn-xs">3</a>
        </div>
        </>
    )
}

const MSpek = (props) => {
    return (
        <>
        <div className='mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
            <p className='font-medium italic text-xs text-slate-500'>{props.label}</p>
            <p className='font-normal text-base text-start'>{props.text}</p>
        </div>
        </>
    )
}

const MNavigationBtn = (props) => {
    return (
        <>
        <a href={props.link}>
            <div className='bg-sky-700 fixed bottom-5 right-5 w-12 h-12 text-slate-100 rounded-full flex justify-center items-center'>
                <FontAwesomeIcon icon={props.icon} />
            </div>
        </a>
        </>
    )
}

export {Navbar, Footer, Box, Box2, MCarousel, MSpek, MNavigationBtn};