import {images, dummy} from '../assets/index';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const Navbar = () => {
    return (
        <>
        <div className="w-full flex justify-between items-center bg-sky-100 px-20 py-2" id="nav">
            <div>
                <a href="#nav">
                <img className='w-[50px]' src={images.ImgNavbar} alt="Logo Sepeda" />
                </a>
            </div>
            <div className="sm:w-1/2 w-2/6 flex justify-between ">
                <a href="#road">
                    <p className='sm:text-sm lg:text-base font-medium hover:font-bold'>Road Bike</p>
                </a>
                <a href="#mountain">
                    <p className='sm:text-sm lg:text-base font-medium hover:font-bold'>Mountain Bike</p>
                </a>
                <a href="#folding">
                    <p className='sm:text-sm lg:text-base font-medium hover:font-bold'>Folding Bike</p>
                </a>
            </div>
        </div>
        </>
    )
}

const DCarousel = (props) => {
    return (
        <>
        <div className='w-full flex justify-center items-center sm:my-10 lg:my-20'>
            <div className="carousel w-4/5 rounded-xl drop-shadow-xl">
                <div id="slide1" className="carousel-item relative w-full">
                    <img src={props.img1} className="w-full" />
                    <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                    <a href="#slide3" className="btn btn-circle">❮</a> 
                    <a href="#slide2" className="btn btn-circle">❯</a>
                    </div>
                </div> 
                <div id="slide2" className="carousel-item relative w-full">
                    <img src={props.img2} className="w-full" />
                    <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                    <a href="#slide1" className="btn btn-circle">❮</a> 
                    <a href="#slide3" className="btn btn-circle">❯</a>
                    </div>
                </div> 
                <div id="slide3" className="carousel-item relative w-full">
                    <img src={props.img3} className="w-full" />
                    <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                    <a href="#slide2" className="btn btn-circle">❮</a> 
                    <a href="#slide1" className="btn btn-circle">❯</a>
                    </div>
                </div>
            </div>

        </div>

        </>
    )
}

const DSepeda = (props) => {
    return (
        <>
        <div className={'w-full py-10 flex items-center justify-center ' + props.bg} id={props.id}>
            <div className='w-4/5 flex flex-col justify-center items-center'>
                <p className='text-2xl font-semibold'>{dummy[props.bike].title}</p>
                <p className='text-justify mt-3'>{dummy[props.bike].description}</p>

                <div className='flex flex-row gap-10 mt-5'>
                    <label htmlFor={props.mlink1}>
                        <div className='flex flex-col items-center hover:cursor-pointer'>
                            <div className='w-[200px] h-[200px] border-2 border-solid border-primary-focus rounded-lg'>
                                <img src={dummy[props.bike].sepeda.s1.img1} alt={dummy[props.bike].sepeda.s1.name} />
                            </div>
                            <p className='font-semibold text-base'>{dummy[props.bike].sepeda.s1.name}</p>
                        </div>
                    </label>
                    <label htmlFor={props.mlink2}>
                        <div className='flex flex-col items-center hover:cursor-pointer'>
                            <div className='w-[200px] h-[200px] border-2 border-solid border-primary-focus rounded-lg'>
                                <img src={dummy[props.bike].sepeda.s2.img1} alt={dummy[props.bike].sepeda.s2.name} />
                            </div>
                            <p className='font-semibold text-base'>{dummy[props.bike].sepeda.s2.name}</p>
                        </div>
                    </label>
                    <label htmlFor={props.mlink3}>
                        <div className='flex flex-col items-center hover:cursor-pointer'>
                            <div className='w-[200px] h-[200px] border-2 border-solid border-primary-focus rounded-lg'>
                                <img src={dummy[props.bike].sepeda.s3.img1} alt={dummy[props.bike].sepeda.s3.name} />
                            </div>
                            <p className='font-semibold text-base'>{dummy[props.bike].sepeda.s3.name}</p>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        </>
    )
}

const DModal = (props) => {
    return (
        <>
        <input type="checkbox" id={props.id} className="modal-toggle" />
        <div className="modal">
        <div className="modal-box relative w-11/12 max-w-5xl">
            <label htmlFor={props.id} className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
            <h3 className="text-lg font-bold">{dummy[props.bike].sepeda[props.urut].name}</h3>
            <div className='flex sm:flex-col sm:items-center lg:flex-row gap-5'>
                <div>
                    <div className="carousel w-[400px]">
                        <div id={props.id + '1'} className="carousel-item w-full">
                            <img src={dummy[props.bike].sepeda[props.urut].img1} className="w-full rounded-lg" />
                        </div> 
                        <div id={props.id + '2'} className="carousel-item w-full">
                            <img src={dummy[props.bike].sepeda[props.urut].img2} className="w-full rounded-lg" />
                        </div> 
                        <div id={props.id + '3'} className="carousel-item w-full">
                            <img src={dummy[props.bike].sepeda[props.urut].img3} className="w-full rounded-lg" />
                        </div>
                    </div> 
                    <div className="flex justify-center w-full py-2 gap-2">
                        <a href={'#' +props.id + '1'} className="btn btn-xs">1</a> 
                        <a href={'#' +props.id + '2'} className="btn btn-xs">2</a> 
                        <a href={'#' +props.id + '3'} className="btn btn-xs">3</a>
                    </div>
                </div>
                <div className='w-full flex flex-row gap-10 mr-5'>
                    <div className='w-1/2 flex flex-col'>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Frame</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].frame}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Fork</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].fork}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Shifter</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].shifter}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>{props.bike === 'folding' ? 'External Gear' : 'Front Derailleur'}</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].fd}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>{props.bike === 'folding' ? 'Internal Gear' : 'Rear Derailleur'}</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].rd}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Crankset</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].crankset}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Sprocket</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].sprocket}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Chain</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].chain}</p>
                        </div>

                    </div>
                    <div className='w-1/2 flex flex-col'>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Pedal</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].pedal}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Front Brake</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].fb}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Rear Brake</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].rb}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Handle Bar</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].handle}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Saddle</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].saddle}</p>
                        </div>
                        {props.bike === 'folding' ? (
                            <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                                <p className='font-medium italic text-xs text-slate-500'>Seatpost</p>
                                <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].seatpost}</p>
                            </div>
                        ) : ''}
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Rims</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].rims}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Tires</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].tires}</p>
                        </div>
                        <div className='w-full mx-6 mt-2 flex flex-col justify-start items-start border-b-2'>
                            <p className='font-medium italic text-xs text-slate-500'>Spokes</p>
                            <p className='font-normal sm:text-sm lg:text-base text-start'>{dummy[props.bike].sepeda[props.urut].spokes}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
        </>
    )
}

const DFooter = () => {
    return (
        <>
        <footer className="footer p-10 bg-neutral text-neutral-content">
            <div>
                <img className='w-[50px]' src={images.ImgNavbar} alt="" />
                <p className="font-bold">
                Website ini ditujukan hanya untuk portopolio. <br/>Logo, Gambar dan Deskripsi diambil dari <a href="https://elementbike.id" target="_blank"><i>Element Bike</i></a>
                </p> 
            </div> 
            <div>
                <span className="footer-title">Social</span> 
                <div className="grid grid-flow-col gap-4">
                    <a href='https://twitter.com/_jafarsodiq'><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-current"><path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"></path></svg></a> 
                    <a><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className="fill-current"><path d="M22.23 0H1.77C.8 0 0 .77 0 1.72v20.56C0 23.23.8 24 1.77 24h20.46c.98 0 1.77-.77 1.77-1.72V1.72C24 .77 23.2 0 22.23 0zM7.27 20.1H3.65V9.24h3.62V20.1zM5.47 7.76h-.03c-1.22 0-2-.83-2-1.87 0-1.06.8-1.87 2.05-1.87 1.24 0 2 .8 2.02 1.87 0 1.04-.78 1.87-2.05 1.87zM20.34 20.1h-3.63v-5.8c0-1.45-.52-2.45-1.83-2.45-1 0-1.6.67-1.87 1.32-.1.23-.11.55-.11.88v6.05H9.28s.05-9.82 0-10.84h3.63v1.54a3.6 3.6 0 0 1 3.26-1.8c2.39 0 4.18 1.56 4.18 4.89v6.21z"/></svg></a>
                </div>
            </div>
        </footer>
        </>
    )
}

const DNavigationBtn = (props) => {
    return (
        <>
        <a href={props.link}>
            <div className='bg-sky-700 fixed bottom-5 right-5 w-12 h-12 text-slate-100 rounded-full flex justify-center items-center'>
                <FontAwesomeIcon icon={props.icon} />
            </div>
        </a>
        </>
    )
}

export {Navbar, DCarousel, DSepeda, DModal, DFooter, DNavigationBtn}