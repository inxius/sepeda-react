import { useRouteError } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faTriangleExclamation, faFaceSadTear } from "@fortawesome/free-solid-svg-icons"

function ErrorPage() {
    const err = useRouteError()

    return (
        <>
        <div className="w-screen h-screen flex flex-col justify-center items-center gap-2">
            <p className="text-5xl">
                <FontAwesomeIcon icon={faTriangleExclamation} />
            </p>
            <h1>Hey! What are you doing here?</h1>
            <p className="text-sm">Oops! Seems there is some error has occured <FontAwesomeIcon icon={faFaceSadTear} /> </p>
            <p className="italic font-semibold text-sm">{err.statusText || err.message}</p>
            <a href="/" className="btn btn-sm btn-info">Bring me back</a>
        </div>
        </>
    )
}

export default ErrorPage