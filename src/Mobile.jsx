import {Navbar, Footer, Box, Box2, MCarousel, MSpek, MNavigationBtn} from './components/mobile.components'
import {images, dummy} from './assets'
import { useParams } from 'react-router-dom';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';

function MobileMainView() {
    return (
        <>
        <div className='h-screen w-screen m-0 p-0'>
            <Navbar />

            <div className='flex flex-col justify-center items-center gap-2 mt-5'>
                <Box img={images.MRoadBike} alt="Road Bike" link="/road" />
                <Box img={images.MMountainBike} alt="Mountain Bike" link="/mountain" />
                <Box img={images.MFoldingBike} alt="Folded Bike" link="/folding" />
            </div>
        </div>
        </>
    )
}

const MBikeView = () => {

    let {bike} = useParams();

    return (
        <>
        <Navbar />

        <div className='flex flex-col gap-2 justify-center items-center mt-3'>
            <p className='font-semibold text-xl'>{dummy[bike].title}</p>
            <p className='font-normal text-sm mx-6 text-justify'>
                {dummy[bike].description}
            </p>

            <Box2 img={dummy[bike].sepeda.s1.img1} alt="image 1" title={dummy[bike].sepeda.s1.name} link={'/' + bike + '/s1'} />
            <Box2 img={dummy[bike].sepeda.s2.img1} alt="image 2" title={dummy[bike].sepeda.s2.name} link={'/' + bike + '/s2'} />
            <Box2 img={dummy[bike].sepeda.s3.img1} alt="image 3" title={dummy[bike].sepeda.s3.name} link={'/' + bike + '/s3'} />
        </div>

        <Footer />
        </>
    )
}

const MBikeDetailView = () => {
    let {bike, id} = useParams();

    return (
        <>
        <Navbar />

        <div className='flex flex-col gap-5 justify-center items-center'>
            <p className='font-semibold text-base mt-5'>{dummy[bike].sepeda[id].name}</p>
            <MCarousel
                img1={dummy[bike].sepeda[id].img1}
                img2={dummy[bike].sepeda[id].img2}
                img3={dummy[bike].sepeda[id].img3}
            />

            <p className='font-normal text-sm mx-6 text-justify'>{dummy[bike].sepeda[id].description}</p>

        </div>

        <div className='w-[350px] mt-5 flex flex-col'>
            <p className='text-start mx-6 mb-2 font-semibold'>Spesification:</p>
            <MSpek label="Frame" text={dummy[bike].sepeda[id].frame} />
            <MSpek label="Fork" text={dummy[bike].sepeda[id].fork} />
            <MSpek label="Shifter" text={dummy[bike].sepeda[id].shifter} />
            <MSpek label={bike === 'folding' ? 'External Gear' : 'Front Derailleur'} text={dummy[bike].sepeda[id].fd} />
            <MSpek label={bike === 'folding' ? 'Internal Gear' : 'Rear Derailleur'} text={dummy[bike].sepeda[id].rd} />
            <MSpek label="Crankset" text={dummy[bike].sepeda[id].crankset} />
            <MSpek label="Sprocket" text={dummy[bike].sepeda[id].sprocket} />
            <MSpek label="Chain" text={dummy[bike].sepeda[id].chain} />
            <MSpek label="Pedal" text={dummy[bike].sepeda[id].pedal} />
            <MSpek label="Front Brake" text={dummy[bike].sepeda[id].fb} />
            <MSpek label="Rear Brake" text={dummy[bike].sepeda[id].rb} />
            <MSpek label="Handle Bar" text={dummy[bike].sepeda[id].handle} />
            <MSpek label="Saddle" text={dummy[bike].sepeda[id].saddle} />
            {bike === 'folding' ? (
                <MSpek label="Seatpost" text={dummy[bike].sepeda[id].seatpost} />
            ) : ''}
            <MSpek label="Rims" text={dummy[bike].sepeda[id].rims} />
            <MSpek label="Tires" text={dummy[bike].sepeda[id].tires} />
            <MSpek label="Spokes" text={dummy[bike].sepeda[id].spokes} />

        </div>
        
        <MNavigationBtn link="#nav" icon={faArrowUp} />
        <Footer />
        </>
    )
}

export {MobileMainView, MBikeView, MBikeDetailView}